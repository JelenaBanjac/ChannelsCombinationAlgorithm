################################################################################
##*/**
##** @file bpcmGlobalConsts.h
##**
##** Module:
##** Description:
##** $Source: $
##** $Revision: $
##** $Date: $
##** $Author: $
##**
##**/
################################################################################
  .list -cond
  .if !defined(_mtechoGlobalConsts_h_)
_mtechoGlobalConsts_h_


#	Public Constants
#-------------------------------------------------------------------------------
#TODO: Insert constant definitions:
BLOCK_SIZE								.equ 16
MAX_NUM_CHANNEL							.equ 8
BUFFER_MAX_LENGTH 						.equ 10
N_DYNAMIC_LOSS							.equ 3
N_STATIC_LOSS							.equ 4
N_COEFFS								.equ 41
OUTPUTS_NUM			 					.equ 10
#...
################################################################################
  .endif
