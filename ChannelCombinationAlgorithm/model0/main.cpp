/////////////////////////////////////////////////////////////////////////////////
//
// @file model0.cpp
//
// Module: channelsCombinationAlgorithm
// Description:  
// $Source: $
// $Revision: 1.0 $
// $Date: 3.10.2017 $
// $Author: Jelena Banjac $
//
/////////////////////////////////////////////////////////////////////////////////
#include <stdlib.h>
#include <string.h>
#include <math.h> 
#include "WAVheader.h"

/////////////////////////////////////////////////////////////////////////////////
// Constant definitions
/////////////////////////////////////////////////////////////////////////////////
#define BLOCK_SIZE 16
#define MAX_NUM_CHANNEL 8
#define BUFFER_MAX_LENGTH 10
#define N_DYNAMIC_LOSS 3
#define N_STATIC_LOSS 4
#define N_COEFFS 41
/////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////
// IO buffers
/////////////////////////////////////////////////////////////////////////////////
double sampleBuffer[MAX_NUM_CHANNEL][BLOCK_SIZE];
/////////////////////////////////////////////////////////////////////////////////
/*const double coeffs[N_COEFFS] = {0.000281, 0.000557, 0.000934, 0.001422, 0.002031, 0.002764, 0.003620, 0.004592, 0.005669, 0.006831, 
								0.008055, 0.009313, 0.010572, 0.011798, 0.012955, 0.014006, 0.014919, 0.015664, 0.016214, 0.016553, 
								0.016667, 0.016553, 0.016214, 0.015664, 0.014919, 0.014006, 0.012955, 0.011798, 0.010572, 0.009313, 
								0.008055, 0.006831, 0.005669, 0.004592, 0.003620, 0.002764, 0.002031, 0.001422, 0.000934, 0.000557, 
								0.000281}; */
const double coeffs[N_COEFFS] = {0.000084, 0.000163, 0.000269, 0.000404, 0.000568, 0.000763, 0.000986, 0.001236, 0.001509, 0.001800, 
								0.002103, 0.002411, 0.002717, 0.003012, 0.003289, 0.003539,0.003756, 0.003931, 0.004061, 0.004140, 
								0.004167, 0.004140, 0.004061, 0.003931, 0.003756, 0.003539, 0.003289, 0.003012, 0.002717, 0.002411, 
								0.002103, 0.001800, 0.001509, 0.001236, 0.000986, 0.000763, 0.000568, 0.000404, 0.000269, 0.000163, 
								0.000084};

double history;
int p_state;


/////////////////////////////////////////////////////////////////////////////////
// Output Mode
/////////////////////////////////////////////////////////////////////////////////
typedef enum  
{
	OUTPUT_MODE_3_2_0 = 0,	//LR, C, LsRs
	OUTPUT_MODE_2_0_0,		//LR  
	OUTPUT_MODE_2_0_1,		//LR, LFE
	OUTPUT_MODE_3_2_1		//LR c, LsRs, LFE
} OutputMode;
/////////////////////////////////////////////////////////////////////////////////

OutputMode outputMode;

/////////////////////////////////////////////////////////////////////////////////
// Control state structure
/////////////////////////////////////////////////////////////////////////////////
typedef struct  
{
	double* pChannelCombinationBuff;
	int bufferLength;
	double dynamic_loss[N_DYNAMIC_LOSS];
	int n_dynamic_loss;
	double static_loss[N_STATIC_LOSS];
	int n_static_loss;
} ChannelCombinationState;
/////////////////////////////////////////////////////////////////////////////////

typedef struct  
{
	// System inputs
	double* pInBufLeft;
	double* pInBufRight;
	// System outputs
	double* pOutBufLs;
	double* pOutBufL;
	double* pOutBufC;
	double* pOutBufR;
	double* pOutBufRs;
	double* pOutBufLFE;
	// Lenght of these samples in inputn (and output as well)
	int bufferLength;
} ChannelCombination;

double fir_circular(double input, double *history, int *p_state) 
{
	int i;
	int state;
	double ret_val;
	
	state = *p_state;
	history[state] = input;
	if (++state >= N_COEFFS)
	{
		state = 0;
	}
	ret_val = 0.0;
	for (i = N_COEFFS - 1; i >= 0; i --)
	{
		ret_val += coeffs[i] * history[state];
		if (++state >= N_COEFFS)
		{
			state = 0;
		}
	}

	*p_state = state;

	return ret_val;
}

/////////////////////////////////////////////////////////////////////////////////
// @Author	Jelena Banjac
// @Date	3.10.2017
//
// Function:
// multitap_echo_init
//
// @param - echoState - Control state structure
//		  - buffer - buffer for keeping delayed samples
//		  - echoBufLen - Length of buffer
//		  - delay - array containing delay values in number of samples
//		  - input_gain - gain to be applayed to input sample
//		  - tap_gain - array of gains to be applayed to each delayed sample
//		  - n_tap - number of taps (equals length of delay and tap_gain)
//
// @return - nothing
// Comment: Initialize echoState structure
//
// E-mail:	<email>
//
/////////////////////////////////////////////////////////////////////////////////
void channel_combination_state_init(ChannelCombinationState* channelCombinationState, double* buffer, const int bufferLen, const double dynamic_loss[], const int n_dynamic_loss, const double static_loss[], const int n_static_loss)
{
	int i;
	for (i = 0; i < bufferLen; i++)
	{
		buffer[i] = 0.0;
	}
	channelCombinationState->pChannelCombinationBuff = buffer;
	channelCombinationState->bufferLength = bufferLen;

	for(i = 0; i< n_dynamic_loss; i++)
	{
		channelCombinationState->dynamic_loss[i] = dynamic_loss[i];
	}
	for(i = 0; i< n_static_loss; i++)
	{
		channelCombinationState->static_loss[i] = static_loss[i];
	}
	
}

void channel_combination_init(ChannelCombination* channelCombination, 
						double* inBufLeft, double* outBufC, double* inBufRight, double* outBufLs, double* outBufRs, double* outBufLFE, int buffLen)
{
	channelCombination->pInBufLeft = inBufLeft;
	channelCombination->pInBufRight = inBufRight;
	channelCombination->pOutBufLs = outBufLs;
	channelCombination->pOutBufL = inBufLeft;
	channelCombination->pOutBufC = outBufC;
	channelCombination->pOutBufR = inBufRight;
	channelCombination->pOutBufRs = outBufRs;
	channelCombination->pOutBufLFE = outBufLFE;
	channelCombination->bufferLength = buffLen;
}


/////////////////////////////////////////////////////////////////////////////////
// @Author	Jelena Banjac
// @Date	3.10.2017 
//
// Function:
// multitap_echo
//
// @param - pInbuf - Buffer with input samples
//		  - pOutbuf - Buffer with output samples
//		  - inputLen - Length of input and output buffer
//		  - echoState - Control state structure
//
// @return - nothing
// Comment: Apply echo to input samples
//
// E-mail:	<email>
//
/////////////////////////////////////////////////////////////////////////////////
void channel_combination(double* inBufLeft, double* outBufL, double* outBufC, double* inBufRight,
		double* outBufR, double* outBufLs, double* outBufRs,double* outBufLFE, int bufferLength, ChannelCombinationState* channelCombinationState)
{
	
	int i, j;

	for(i = 0; i < bufferLength; i++)
	{
		

		channelCombinationState->pChannelCombinationBuff[0] = inBufLeft[i] * channelCombinationState->dynamic_loss[0];
		channelCombinationState->pChannelCombinationBuff[1] = inBufRight[i] * channelCombinationState->dynamic_loss[1];
		channelCombinationState->pChannelCombinationBuff[2] = channelCombinationState->pChannelCombinationBuff[0] + channelCombinationState->pChannelCombinationBuff[1];
		channelCombinationState->pChannelCombinationBuff[3] = channelCombinationState->pChannelCombinationBuff[2] * channelCombinationState->dynamic_loss[2];
		channelCombinationState->pChannelCombinationBuff[4] = channelCombinationState->pChannelCombinationBuff[0] * channelCombinationState->static_loss[0];
		channelCombinationState->pChannelCombinationBuff[5] = channelCombinationState->pChannelCombinationBuff[3] * channelCombinationState->static_loss[1];
		channelCombinationState->pChannelCombinationBuff[6] = channelCombinationState->pChannelCombinationBuff[3] * channelCombinationState->static_loss[2];
		channelCombinationState->pChannelCombinationBuff[7] = channelCombinationState->pChannelCombinationBuff[1] * channelCombinationState->static_loss[3];
		channelCombinationState->pChannelCombinationBuff[8] = channelCombinationState->pChannelCombinationBuff[4] + channelCombinationState->pChannelCombinationBuff[5];
		channelCombinationState->pChannelCombinationBuff[9] = channelCombinationState->pChannelCombinationBuff[6] + channelCombinationState->pChannelCombinationBuff[7];
		
		// Ls buffer output
		outBufLs[i] = channelCombinationState->pChannelCombinationBuff[8];
		// L buffer output
		outBufL[i] = channelCombinationState->pChannelCombinationBuff[5];
		// C buffer output
		outBufC[i] = channelCombinationState->pChannelCombinationBuff[3];
		// R buffer output
		outBufR[i] = channelCombinationState->pChannelCombinationBuff[6];
		// Rs buffer output
		outBufRs[i] = channelCombinationState->pChannelCombinationBuff[9];

		// LFE
		outBufLFE[i] = fir_circular(outBufC[i], &history, &p_state);
		
		switch (outputMode) 
		{
		case OUTPUT_MODE_3_2_0:		// L R C Ls Rs
			outBufLFE[i] = 0;
			break;
		case OUTPUT_MODE_2_0_0:		// L R
			outBufC[i] = 0;
			outBufLs[i] = 0;
			outBufRs[i] = 0;
			outBufLFE[i] = 0;
			break;
		case OUTPUT_MODE_2_0_1:		// L R LFE
			outBufC[i] = 0;
			outBufLs[i] = 0;
			outBufRs[i] = 0;
			break;
		case OUTPUT_MODE_3_2_1:		// L R C Ls Rs LFE
			//channelCombination->pOutBufLFE[i] = 0;
			break;
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////
// @Author	Jelena Banjac
// @Date	3.10.2017
//
// Function:
// main
//
// @param - argv[1] - Input file name
//        - argv[2] - Output file name
//		  - argv[3] - Input gain left
//		  - argv[4] - Input gain right
//		  - argv[5] - Headroom gain
// @return - nothing
// Comment: main routine of a program
//
// E-mail:	<email>
//
/////////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
	FILE *wav_in=NULL;
	FILE *wav_out=NULL;
	char WavInputName[256];
	char WavOutputName[256];
	WAV_HEADER inputWAVhdr,outputWAVhdr;	


	// Multitap loss state and initialization constants
	//-------------------------------------------------
	ChannelCombinationState channelCombinationState;
	double channel_combination_buffer[BUFFER_MAX_LENGTH];
	// Dynamic loss/gain values
	const double initial_input_loss_left = pow(10.0, atof(argv[3])/20.0);	// -6dB default
	const double initial_input_loss_right = pow(10.0, atof(argv[4])/20.0);	// -6dB default
	const double initial_headroom_loss = pow(10.0, atof(argv[5])/20.0);		// -6dB default
	const double dynamic_loss[N_DYNAMIC_LOSS] = {initial_input_loss_left, initial_input_loss_right, initial_headroom_loss};
	// Static loss/gain values
	const double loss_2_dB = 0.7943282347242815;	// -2dB
	const double loss_3_dB = 0.7079457843841379;	// -3dB
	const double loss_4_dB = 0.6309573444801932;	// -4dB
	const double loss_1_dB = 0.8912509381337456;	// -1dB
	const double static_loss[N_STATIC_LOSS] = {loss_2_dB, loss_3_dB, loss_4_dB, loss_1_dB};
	// Output mode
	outputMode = (OutputMode)atoi(argv[6]);
	//-------------------------------------------------

	// Init channel buffers
	for(int i=0; i<MAX_NUM_CHANNEL; i++)
		memset(&sampleBuffer[i],0,BLOCK_SIZE);

	// Open input and output wav files
	//-------------------------------------------------
	strcpy(WavInputName,argv[1]);
	wav_in = OpenWavFileForRead (WavInputName,"rb");
	strcpy(WavOutputName,argv[2]);
	wav_out = OpenWavFileForRead (WavOutputName,"wb");
	//-------------------------------------------------

	// Read input wav header
	//-------------------------------------------------
	ReadWavHeader(wav_in,inputWAVhdr);
	//-------------------------------------------------
	
	// Set up output WAV header
	//-------------------------------------------------	
	outputWAVhdr = inputWAVhdr;
	outputWAVhdr.fmt.NumChannels = 6; // change number of channels

	int oneChannelSubChunk2Size = inputWAVhdr.data.SubChunk2Size/inputWAVhdr.fmt.NumChannels;
	int oneChannelByteRate = inputWAVhdr.fmt.ByteRate/inputWAVhdr.fmt.NumChannels;
	int oneChannelBlockAlign = inputWAVhdr.fmt.BlockAlign/inputWAVhdr.fmt.NumChannels;
	
	outputWAVhdr.data.SubChunk2Size = oneChannelSubChunk2Size*outputWAVhdr.fmt.NumChannels;
	outputWAVhdr.fmt.ByteRate = oneChannelByteRate*outputWAVhdr.fmt.NumChannels;
	outputWAVhdr.fmt.BlockAlign = oneChannelBlockAlign*outputWAVhdr.fmt.NumChannels;


	// Write output WAV header to file
	//-------------------------------------------------
	WriteWavHeader(wav_out,outputWAVhdr);

	
	// Initialize echo 
	channel_combination_state_init(&channelCombinationState, 
		channel_combination_buffer, BUFFER_MAX_LENGTH,
		dynamic_loss, N_DYNAMIC_LOSS, 
		static_loss, N_STATIC_LOSS);
    

	// Processing loop
	//-------------------------------------------------	
	{
		int sample;
		int BytesPerSample = inputWAVhdr.fmt.BitsPerSample/8;
		const double SAMPLE_SCALE = -(double)(1 << 31);		//2^31
		int iNumSamples = inputWAVhdr.data.SubChunk2Size/(inputWAVhdr.fmt.NumChannels*inputWAVhdr.fmt.BitsPerSample/8);
		
		// exact file length should be handled correctly...
		for(int i=0; i<iNumSamples/BLOCK_SIZE; i++)
		{	
			for(int j=0; j<BLOCK_SIZE; j++)
			{
				for(int k=0; k<inputWAVhdr.fmt.NumChannels; k++)
				{	
					sample = 0; //debug
					fread(&sample, BytesPerSample, 1, wav_in);
					sample = sample << (32 - inputWAVhdr.fmt.BitsPerSample); // force signextend
					sampleBuffer[k][j] = sample / SAMPLE_SCALE;				// scale sample to 1.0/-1.0 range		
				}
			}
			// Initialize input and output buffers
			ChannelCombination channelCombination;
			/*channelCombination->pInBufLeft = sampleBuffer[0];
			channelCombination->pInBufRight = sampleBuffer[2];
			channelCombination->pOutBufLs = sampleBuffer[3];
			channelCombination->pOutBufL = sampleBuffer[0];
			channelCombination->pOutBufC = sampleBuffer[1];
			channelCombination->pOutBufR = sampleBuffer[2];
			channelCombination->pOutBufRs = sampleBuffer[4];
			channelCombination->pOutBufLFE = sampleBuffer[5];
			channelCombination->bufferLength = BLOCK_SIZE;*/ 

			// Initialize echo 
			channel_combination_init(&channelCombination, 
				sampleBuffer[0],sampleBuffer[1],sampleBuffer[2],sampleBuffer[3],sampleBuffer[4],sampleBuffer[5],BLOCK_SIZE);

			// Call processing on first channel
			channel_combination(sampleBuffer[0],sampleBuffer[0],sampleBuffer[1],sampleBuffer[2],sampleBuffer[2],sampleBuffer[3],sampleBuffer[4],sampleBuffer[5],BLOCK_SIZE, &channelCombinationState);
			
			for(int j=0; j<BLOCK_SIZE; j++)
			{
				for(int k=0; k<outputWAVhdr.fmt.NumChannels; k++)
				{	
					sample = sampleBuffer[k][j] * SAMPLE_SCALE ;	// crude, non-rounding 			
					sample = sample >> (32 - inputWAVhdr.fmt.BitsPerSample);
					fwrite(&sample, outputWAVhdr.fmt.BitsPerSample/8, 1, wav_out);		
				}
			}		
		}
	}
	
	// Close files
	//-------------------------------------------------	
	fclose(wav_in);
	fclose(wav_out);
	//-------------------------------------------------	

	return 0;
}