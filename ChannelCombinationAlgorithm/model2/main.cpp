/////////////////////////////////////////////////////////////////////////////////
//
// @file model0.cpp
//
// Module: channelsCombinationAlgorithm
// Description:  
// $Source: $
// $Revision: 1.0 $
// $Date: 3.10.2017 $
// $Author: Jelena Banjac $
//
/////////////////////////////////////////////////////////////////////////////////
#include <stdlib.h>
#include <string.h>
#include <math.h> 
#include "WAVheader.h"
#include "stdfix_emu.h"
#include "fixed_point_math.h"
#include "common.h"

/////////////////////////////////////////////////////////////////////////////////
// IO buffers
/////////////////////////////////////////////////////////////////////////////////
DSPfract  sampleBuffer[MAX_NUM_CHANNEL][BLOCK_SIZE];
DSPfract  output[10];
/*const double coeffs[N_COEFFS] = {0.000281, 0.000557, 0.000934, 0.001422, 0.002031, 0.002764, 0.003620, 0.004592, 0.005669, 0.006831, 
								0.008055, 0.009313, 0.010572, 0.011798, 0.012955, 0.014006, 0.014919, 0.015664, 0.016214, 0.016553, 
								0.016667, 0.016553, 0.016214, 0.015664, 0.014919, 0.014006, 0.012955, 0.011798, 0.010572, 0.009313, 
								0.008055, 0.006831, 0.005669, 0.004592, 0.003620, 0.002764, 0.002031, 0.001422, 0.000934, 0.000557, 
								0.000281}; */
/*const DSPfract coeffs[N_COEFFS] = {FRACT_NUM(0.000084), FRACT_NUM(0.000163), FRACT_NUM(0.000269), FRACT_NUM(0.000404), FRACT_NUM(0.000568), FRACT_NUM(0.000763), FRACT_NUM(0.000986), FRACT_NUM(0.001236), FRACT_NUM(0.001509), FRACT_NUM(0.001800), 
								FRACT_NUM(0.002103), FRACT_NUM(0.002411), FRACT_NUM(0.002717), FRACT_NUM(0.003012), FRACT_NUM(0.003289), FRACT_NUM(0.003539), FRACT_NUM(0.003756), FRACT_NUM(0.003931), FRACT_NUM(0.004061), FRACT_NUM(0.004140), 
								FRACT_NUM(0.004167), FRACT_NUM(0.004140), FRACT_NUM(0.004061), FRACT_NUM(0.003931), FRACT_NUM(0.003756), FRACT_NUM(0.003539), FRACT_NUM(0.003289), FRACT_NUM(0.003012), FRACT_NUM(0.002717), FRACT_NUM(0.002411), 
								FRACT_NUM(0.002103), FRACT_NUM(0.001800), FRACT_NUM(0.001509), FRACT_NUM(0.001236), FRACT_NUM(0.000986), FRACT_NUM(0.000763), FRACT_NUM(0.000568), FRACT_NUM(0.000404), FRACT_NUM(0.000269), FRACT_NUM(0.000163), 
								FRACT_NUM(0.000084)};*/
const DSPfract coeffs[N_COEFFS] = {0.000084, 0.000163, 0.000269, 0.000404, 0.000568, 0.000763, 0.000986, 0.001236, 0.001509, 0.001800, 
								0.002103, 0.002411, 0.002717, 0.003012, 0.003289, 0.003539,0.003756, 0.003931, 0.004061, 0.004140, 
								0.004167, 0.004140, 0.004061, 0.003931, 0.003756, 0.003539, 0.003289, 0.003012, 0.002717, 0.002411, 
								0.002103, 0.001800, 0.001509, 0.001236, 0.000986, 0.000763, 0.000568, 0.000404, 0.000269, 0.000163, 
								0.000084};
/////////////////////////////////////////////////////////////////////////////////

DSPfract  history;
DSPint  p_state;


/////////////////////////////////////////////////////////////////////////////////
// Output Mode
/////////////////////////////////////////////////////////////////////////////////
typedef enum  
{
	OUTPUT_MODE_3_2_0 = 0,	//LR, C, LsRs
	OUTPUT_MODE_2_0_0,		//LR  
	OUTPUT_MODE_2_0_1,		//LR, LFE
	OUTPUT_MODE_3_2_1		//LR c, LsRs, LFE
} OutputMode;
/////////////////////////////////////////////////////////////////////////////////

OutputMode outputMode;

/////////////////////////////////////////////////////////////////////////////////
// Control state structure
/////////////////////////////////////////////////////////////////////////////////
typedef struct  
{
	DSPfract * pChannelCombinationBuff;
	DSPint bufferLength;
	DSPfract  dynamic_loss[N_DYNAMIC_LOSS];
	DSPint n_dynamic_loss;
	DSPfract  static_loss[N_STATIC_LOSS];
	DSPint n_static_loss;
} ChannelCombinationState;

typedef struct  
{
	DSPfract * pInBufLeft;		// System inputs
	DSPfract * pInBufRight;	
	DSPfract * pOutBufLs;		// System outputs
	DSPfract * pOutBufL;
	DSPfract * pOutBufC;
	DSPfract * pOutBufR;
	DSPfract * pOutBufRs;
	DSPfract * pOutBufLFE;
	DSPint  bufferLength;		// Lenght of these samples in inputn (and output as well)
} ChannelCombination;
/////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////
// Channel combination loss state and initialization constants
/////////////////////////////////////////////////////////////////////////////////
ChannelCombinationState channelCombinationState;
ChannelCombination channelCombination;
DSPfract channel_combination_buffer[BUFFER_MAX_LENGTH];
DSPfract dynamic_loss[N_DYNAMIC_LOSS];					// Dynamic loss/gain values
const DSPfract static_loss[N_STATIC_LOSS] = {				// Static loss/gain values
	0.7943282347242815, 0.7079457843841379, 0.6309573444801932, 0.8912509381337456	// -2dB, -3dB, -4dB, -1dB
}; 
	

DSPfract fir_circular(DSPfract input, DSPfract *history, DSPint *p_state) 
{
	DSPint i;
	DSPint state;
	DSPfract ret_val;
	
	state = *p_state;
	history[state] = input;
	if (++state >= N_COEFFS)
	{
		state = 0;
	}
	ret_val = FRACT_NUM(0.0);
	for (i = N_COEFFS - 1; i >= 0; i --)
	{
		ret_val += coeffs[i] * history[state];
		if (++state >= N_COEFFS)
		{
			state = 0;
		}
	}

	*p_state = state;

	return ret_val;
}

/////////////////////////////////////////////////////////////////////////////////
// @Author	Jelena Banjac
// @Date	3.10.2017
//
// Function:
// multitap_echo_init
//
// @param - echoState - Control state structure
//		  - buffer - buffer for keeping delayed samples
//		  - echoBufLen - Length of buffer
//		  - delay - array containing delay values in number of samples
//		  - input_gain - gain to be applayed to input sample
//		  - tap_gain - array of gains to be applayed to each delayed sample
//		  - n_tap - number of taps (equals length of delay and tap_gain)
//
// @return - nothing
// Comment: Initialize echoState structure
//
// E-mail:	<email>
//
/////////////////////////////////////////////////////////////////////////////////
void channel_combination_state_init()
{
	DSPint i;
	DSPfract *pInitChannelCombinationBuffer = channel_combination_buffer;
	for (i=0; i<BUFFER_MAX_LENGTH; i++) 
	{
		*pInitChannelCombinationBuffer = FRACT_NUM(0.0);
		pInitChannelCombinationBuffer++;
	}
	channelCombinationState.pChannelCombinationBuff = channel_combination_buffer;
	channelCombinationState.bufferLength = BUFFER_MAX_LENGTH;

	DSPfract *pChannelCombinationDynamicLoss = channelCombinationState.dynamic_loss;
	DSPfract *pInitChannelCombinationDynamicLoss = dynamic_loss;
	for (i=0; i<N_DYNAMIC_LOSS; i++) 
	{
		*(pChannelCombinationDynamicLoss++) = *(pInitChannelCombinationDynamicLoss++);
	}
	channelCombinationState.n_dynamic_loss = N_DYNAMIC_LOSS;

	DSPfract *pChannelCombinationStaticLoss = channelCombinationState.static_loss;
	const DSPfract *pInitChannelCombinationStaticLoss = static_loss;
	for (i=0; i<N_STATIC_LOSS; i++) 
	{
		*(pChannelCombinationStaticLoss++) = *(pInitChannelCombinationStaticLoss++);
	}
	channelCombinationState.n_static_loss = N_STATIC_LOSS;
}

//void channel_combination_init(ChannelCombination* channelCombination, double* inBufLeft, double* outBufC, double* inBufRight, double* outBufLs, double* outBufRs, double* outBufLFE, int buffLen)
void channel_combination_init()
{
	channelCombination.pInBufLeft = sampleBuffer[0];
	channelCombination.pInBufRight = sampleBuffer[2];
	channelCombination.pOutBufLs = sampleBuffer[3];
	channelCombination.pOutBufL = sampleBuffer[0];
	channelCombination.pOutBufC = sampleBuffer[1];
	channelCombination.pOutBufR = sampleBuffer[2];
	channelCombination.pOutBufRs = sampleBuffer[4];
	channelCombination.pOutBufLFE = sampleBuffer[5];
	channelCombination.bufferLength = BLOCK_SIZE;
}


/////////////////////////////////////////////////////////////////////////////////
// @Author	Jelena Banjac
// @Date	3.10.2017 
//
// Function:
// multitap_echo
//
// @param - pInbuf - Buffer with input samples
//		  - pOutbuf - Buffer with output samples
//		  - inputLen - Length of input and output buffer
//		  - echoState - Control state structure
//
// @return - nothing
// Comment: Apply echo to input samples
//
// E-mail:	<email>
//
/////////////////////////////////////////////////////////////////////////////////
void channel_combination()
{
	DSPint i, j;
	DSPfract *out = output;
	DSPfract *pDynamicLoss = channelCombinationState.dynamic_loss;
	DSPfract *pStaticLoss = channelCombinationState.static_loss;
	DSPfract *pInBufLeft = channelCombination.pInBufLeft;
	DSPfract *pInBufRight = channelCombination.pInBufRight;
	DSPfract *pOutBufLs = channelCombination.pOutBufLs;
	DSPfract *pOutBufL = channelCombination.pOutBufL;
	DSPfract *pOutBufC = channelCombination.pOutBufC;
	DSPfract *pOutBufR = channelCombination.pOutBufR;
	DSPfract *pOutBufRs = channelCombination.pOutBufRs;
	DSPfract *pOutBufLFE = channelCombination.pOutBufLFE;
	/*double *pInBufLeft = sampleBuffer[0];
	double *pInBufRight = sampleBuffer[2];
	double *pOutBufLs = sampleBuffer[3];
	double *pOutBufL = sampleBuffer[0];
	double *pOutBufC = sampleBuffer[1];
	double *pOutBufR = sampleBuffer[2];
	double *pOutBufRs = sampleBuffer[4];
	double *pOutBufLFE = sampleBuffer[5];*/

	for(i = 0; i < BLOCK_SIZE; i++)
	{		
		*(out++) = (*(pInBufLeft++)) * (*(pDynamicLoss++));			// state 0
		*(out++) = (*(pInBufRight++)) * (*(pDynamicLoss++));		// state 1
		*(out++) = *(out-2) + *(out-1);						// state 2
		*(out++) = *(out-1) * (*pDynamicLoss);				// state 3
		
		switch (outputMode) 
		{
		case OUTPUT_MODE_3_2_0:		// L R C Ls Rs
		case OUTPUT_MODE_3_2_1:		// L R C Ls Rs LFE
			*(out++) = *(out-4) * (*(pStaticLoss++));		// state 4
			*(out++) = *(out-2) * (*(pStaticLoss++));		// state 5
			*(out++) = *(out-3) * (*(pStaticLoss++));		// state 6
			*(out++) = *(out-6) * (*(pStaticLoss));			// state 7
			*(out++) = *(out-4) + *(out-3);				// state 8
			*out = *(out-3) + *(out-2);					// state 9

			out -= 9;	// Restart
			pStaticLoss -= 3;
			pDynamicLoss -= 2;

			*(pOutBufLs++) = *(out+8);	// Ls buffer output	
			*(pOutBufL++) = *(out+5);	// L buffer output			
			*pOutBufC = *(out+3);	// C buffer output			
			*(pOutBufR++) = *(out+6);	// R buffer output
			*(pOutBufRs++) = *(out+9);	// Rs buffer output

			if (OUTPUT_MODE_3_2_1) 
			{
				*(pOutBufLFE++) = fir_circular(*pOutBufC, &history, &p_state);	// LFE buffer output
				*(pOutBufC++);
			}
			else 
			{
				*(pOutBufC++);
				*(pOutBufLFE++) = FRACT_NUM(0.0);	// LFE buffer output
			}
			break;
		case OUTPUT_MODE_2_0_0:		// L R
		case OUTPUT_MODE_2_0_1:		// L R LFE
			*(out++) = *(out-1) * (*(pStaticLoss+1));		// state 5 (in place of state 4)
			*(out) = *(out-2) * (*(pStaticLoss+2));		// state 6 (in place of state 5)

			out -= 5;	// Restart
			pDynamicLoss -= 2;

			*(pOutBufLs++) = FRACT_NUM(0.0);		// Ls buffer output	
			*(pOutBufL++) = *(out+4);	// L buffer output					
			*(pOutBufR++) = *(out+5);	// R buffer output
			*(pOutBufRs++) = FRACT_NUM(0.0);		// Rs buffer output

			if (OUTPUT_MODE_2_0_1) 
			{
				*pOutBufC = *(out+3);	// C buffer output		
				*(pOutBufLFE++) = fir_circular(*pOutBufC, &history, &p_state); // LFE buffer output
				*(pOutBufC++);
			}
			else 
			{
				*(pOutBufC++) = FRACT_NUM(0.0);		// C buffer output		
				*(pOutBufLFE++) = FRACT_NUM(0.0);	// LFE buffer output
			}
			break;
		}

	}
	//p_state = 0;
	//pInBufLeft = pInBufLeft-16;
	//pInBufRight = pInBufRight-16;
}

/////////////////////////////////////////////////////////////////////////////////
// @Author	Jelena Banjac
// @Date	3.10.2017
//
// Function:
// main
//
// @param - argv[1] - Input file name
//        - argv[2] - Output file name
//		  - argv[3] - Input gain left
//		  - argv[4] - Input gain right
//		  - argv[5] - Headroom gain
// @return - nothing
// Comment: main routine of a program
//
// E-mail:	<email>
//
/////////////////////////////////////////////////////////////////////////////////
int main(DSPint argc, char* argv[])
{
	FILE *wav_in=NULL;
	FILE *wav_out=NULL;
	char WavInputName[256];
	char WavOutputName[256];
	WAV_HEADER inputWAVhdr,outputWAVhdr;	

	int i;

	// Dynamic loss/gain values
	DSPfract *pDynamicLoss = dynamic_loss;
	for (i=0; i<N_DYNAMIC_LOSS; i++)
	{
		*(pDynamicLoss++) = pow(10, atoi(argv[i+3])/20.0);	// Two input gains (args 3,4) and one headroom gain (arg 5)
	}

	outputMode = (OutputMode)atoi(argv[6]);					// Output mode (arg 6)
	
	// Init channel buffers
	for(DSPint i=0; i<MAX_NUM_CHANNEL; i++)
		for(DSPint j=0; j<BLOCK_SIZE; j++)
			sampleBuffer[i][j] = FRACT_NUM(0.0);

	// Open input and output wav files
	//-------------------------------------------------
	strcpy(WavInputName,argv[1]);
	wav_in = OpenWavFileForRead (WavInputName,"rb");
	strcpy(WavOutputName,argv[2]);
	wav_out = OpenWavFileForRead (WavOutputName,"wb");
	//-------------------------------------------------

	// Read input wav header
	//-------------------------------------------------
	ReadWavHeader(wav_in,inputWAVhdr);
	//-------------------------------------------------
	
	// Set up output WAV header
	//-------------------------------------------------	
	outputWAVhdr = inputWAVhdr;
	outputWAVhdr.fmt.NumChannels = 6; // change number of channels

	DSPint oneChannelSubChunk2Size = inputWAVhdr.data.SubChunk2Size/inputWAVhdr.fmt.NumChannels;
	DSPint oneChannelByteRate = inputWAVhdr.fmt.ByteRate/inputWAVhdr.fmt.NumChannels;
	DSPint oneChannelBlockAlign = inputWAVhdr.fmt.BlockAlign/inputWAVhdr.fmt.NumChannels;
	
	outputWAVhdr.data.SubChunk2Size = oneChannelSubChunk2Size*outputWAVhdr.fmt.NumChannels;
	outputWAVhdr.fmt.ByteRate = oneChannelByteRate*outputWAVhdr.fmt.NumChannels;
	outputWAVhdr.fmt.BlockAlign = oneChannelBlockAlign*outputWAVhdr.fmt.NumChannels;


	// Write output WAV header to file
	//-------------------------------------------------
	WriteWavHeader(wav_out,outputWAVhdr);

	
	// Initialize echo 
	channel_combination_state_init();
	

	// Processing loop
	//-------------------------------------------------	
	{
		DSPint sample;
		DSPint BytesPerSample = inputWAVhdr.fmt.BitsPerSample/8;
		const DSPfract SAMPLE_SCALE = -(DSPfract)(1 << 31);		//2^31
		DSPint iNumSamples = inputWAVhdr.data.SubChunk2Size/(inputWAVhdr.fmt.NumChannels*inputWAVhdr.fmt.BitsPerSample/8);
		
		// exact file length should be handled correctly...
		for(DSPint i=0; i<iNumSamples/BLOCK_SIZE; i++)
		{	
			for(DSPint j=0; j<BLOCK_SIZE; j++)
			{
				for(DSPint k=0; k<inputWAVhdr.fmt.NumChannels; k++)
				{	
					sample = 0; //debug
					fread(&sample, BytesPerSample, 1, wav_in);
					sample = sample << (32 - inputWAVhdr.fmt.BitsPerSample); // force signextend
					sampleBuffer[k][j] = sample / SAMPLE_SCALE;				// scale sample to 1.0/-1.0 range		
				}
			}
			channel_combination_init();
			
			// Call processing on first channel
			channel_combination();
			
			for(DSPint j=0; j<BLOCK_SIZE; j++)
			{
				for(DSPint k=0; k<outputWAVhdr.fmt.NumChannels; k++)
				{	
					sample = sampleBuffer[k][j] * SAMPLE_SCALE ;	// crude, non-rounding 	
					//sample = sampleBuffer[k][j].toLong();
					sample = sample >> (32 - inputWAVhdr.fmt.BitsPerSample);
					fwrite(&sample, outputWAVhdr.fmt.BitsPerSample/8, 1, wav_out);		
				}
			}		
		}
	}
	
	// Close files
	//-------------------------------------------------	
	fclose(wav_in);
	fclose(wav_out);
	//-------------------------------------------------	

	return 0;
}