	.public _channelCombination
	.public _channelCombinationState
	.public _channel_combination_buffer
	.public _coeffs
	.public _dynamic_loss
	.public _history
	.public _inputHeadroomLoss
	.public _inputLeftLoss
	.public _inputRightLoss
	.public _output
	.public _outputMode
	.public _p_state
	.public _sampleBuffer
	.public _static_loss
	.extern _channel_combination
	.public _channel_combination_state_init
	.extern _cl_wavread_bits_per_sample
	.extern _cl_wavread_close
	.extern _cl_wavread_frame_rate
	.extern _cl_wavread_number_of_frames
	.extern _cl_wavread_open
	.extern _cl_wavread_recvsample
	.extern _cl_wavwrite_close
	.extern _cl_wavwrite_open
	.extern _cl_wavwrite_sendsample
	.public _fir_circular
	.public _main
	.extern _memset
	.extern _printf
	.extern _strcpy
	.extern __div
	.xdata_ovly
__extractedConst_0_3
	.dw  (0x4026e73d)
	.xdata_ovly
_channelCombination
	.bss (0x9)
	.xdata_ovly
_channelCombinationState
	.bss (0xb)
	.xdata_ovly align 32
_channel_combination_buffer
	.bss (0xa)
	.xdata_ovly
_coeffs
	.dw  (0x2c0a5)
	.dw  (0x55758)
	.dw  (0x8d089)
	.dw  (0xd3cff)
	.dw  (0x129cbb)
	.dw  (0x190082)
	.dw  (0x204f2b)
	.dw  (0x288052)
	.dw  (0x317269)
	.dw  (0x3afb7f)
	.dw  (0x44e93e)
	.dw  (0x4f00ef)
	.dw  (0x5907d9)
	.dw  (0x62b27d)
	.dw  (0x6bc622)
	.dw  (0x73f749)
	.dw  (0x7b139d)
	.dw  (0x80cf9e)
	.dw  (0x851223)
	.dw  (0x87a8d6)
	.dw  (0x888b54)
	.dw  (0x87a8d6)
	.dw  (0x851223)
	.dw  (0x80cf9e)
	.dw  (0x7b139d)
	.dw  (0x73f749)
	.dw  (0x6bc622)
	.dw  (0x62b27d)
	.dw  (0x5907d9)
	.dw  (0x4f00ef)
	.dw  (0x44e93e)
	.dw  (0x3afb7f)
	.xdata_ovly
_dynamic_loss
	.bss (0x3)
	.xdata_ovly
_history
	.bss (0x1)
	.xdata_ovly
_inputHeadroomLoss
	.bss (0x1)
	.xdata_ovly
_inputLeftLoss
	.bss (0x1)
	.xdata_ovly
_inputRightLoss
	.bss (0x1)
	.xdata_ovly
_output
	.bss (0xa)
	.xdata_ovly
_outputMode
	.bss (0x1)
	.xdata_ovly
_p_state
	.bss (0x1)
	.ydata_ovly
_sampleBuffer
	.bss (0x80)
	.xdata_ovly
_static_loss
	.dw  (0x65ac8c2f)
	.dw  (0x5a9df7ac)
	.dw  (0x50c335d4)
	.dw  (0x721482c0)
	.xdata_ovly
_string_const_0
	.dw  (0x45)
	.dw  (0x72)
	.dw  (0x72)
	.dw  (0x6f)
	.dw  (0x72)
	.dw  (0x3a)
	.dw  (0x20)
	.dw  (0x43)
	.dw  (0x6f)
	.dw  (0x75)
	.dw  (0x6c)
	.dw  (0x64)
	.dw  (0x20)
	.dw  (0x6e)
	.dw  (0x6f)
	.dw  (0x74)
	.dw  (0x20)
	.dw  (0x6f)
	.dw  (0x70)
	.dw  (0x65)
	.dw  (0x6e)
	.dw  (0x20)
	.dw  (0x69)
	.dw  (0x6e)
	.dw  (0x70)
	.dw  (0x75)
	.dw  (0x74)
	.dw  (0x20)
	.dw  (0x77)
	.dw  (0x61)
	.dw  (0x76)
	.dw  (0x65)
	.dw  (0x66)
	.dw  (0x69)
	.dw  (0x6c)
	.dw  (0x65)
	.dw  (0x2e)
	.dw  (0xa)
	.dw  (0x0)
	.xdata_ovly
_string_const_1
	.dw  (0x45)
	.dw  (0x72)
	.dw  (0x72)
	.dw  (0x6f)
	.dw  (0x72)
	.dw  (0x3a)
	.dw  (0x20)
	.dw  (0x43)
	.dw  (0x6f)
	.dw  (0x75)
	.dw  (0x6c)
	.dw  (0x64)
	.dw  (0x20)
	.dw  (0x6e)
	.dw  (0x6f)
	.dw  (0x74)
	.dw  (0x20)
	.dw  (0x6f)
	.dw  (0x70)
	.dw  (0x65)
	.dw  (0x6e)
	.dw  (0x20)
	.dw  (0x6f)
	.dw  (0x75)
	.dw  (0x74)
	.dw  (0x70)
	.dw  (0x75)
	.dw  (0x74)
	.dw  (0x20)
	.dw  (0x77)
	.dw  (0x61)
	.dw  (0x76)
	.dw  (0x65)
	.dw  (0x66)
	.dw  (0x69)
	.dw  (0x6c)
	.dw  (0x65)
	.dw  (0x2e)
	.dw  (0xa)
	.dw  (0x0)
	.code_ovly



_channel_combination_state_init:			/* LN: 149 | CYCLE: 0 | RULES: () */ 
	xmem[i7] = i7			# LN: 149 | 
	i7 += 1			# LN: 149 | 
	i7 = i7 + (0x6)			# LN: 149 | 
cline_149_0:			/* LN: 152 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x1)			# LN: 152 | 
	uhalfword(a0) = (_channel_combination_buffer + 0)			# LN: 152 | 
	xmem[i0] = a0			# LN: 152 | 
cline_152_0:			/* LN: 153 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x2)			# LN: 153 | 
	a0 = 0			# LN: 153 | 
	xmem[i0] = a0h			# LN: 153 | 
	do (0xa), label_end_92			# LN: 153 | 
cline_153_0:			/* LN: 155 | CYCLE: 0 | RULES: () */ 
label_begin_92:			/* LN: 153 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x1)			# LN: 155 | 
	i0 = xmem[i0]			# LN: 155 | 
	a0 = 0			# LN: 155 | 
	xmem[i0] = a0h			# LN: 155 | 
cline_155_0:			/* LN: 156 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x1)			# LN: 156 | 
	i0 = xmem[i0]			# LN: 156 | 
	i1 = i7 - (0x1)			# LN: 156 | 
	i0 += 1			# LN: 156 | 
	xmem[i1] = i0			# LN: 156 | 
cline_156_0:			/* LN: 153 | CYCLE: 0 | RULES: () */ 
init_latch_label_1:			/* LN: 157 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x2)			# LN: 153 | 
	a0 = xmem[i0]			# LN: 153 | 
	uhalfword(a1) = (0x1)			# LN: 153 | 
	a0 = a0 + a1			# LN: 153 | 
	i0 = i7 - (0x2)			# LN: 153 | 
label_end_92:			# LN: 153 | CYCLE: 5 | RULES: ()
	xmem[i0] = a0h			# LN: 153 | 
cline_153_1:			/* LN: 158 | CYCLE: 0 | RULES: () */ 
for_end_1:			/* LN: 153 | CYCLE: 0 | RULES: () */ 
	i0 = (0) + (_channel_combination_buffer + 0)			# LN: 158 | 
	xmem[_channelCombinationState + 0] = i0			# LN: 158 | 
cline_158_0:			/* LN: 159 | CYCLE: 0 | RULES: () */ 
	uhalfword(a0) = (0xa)			# LN: 159 | 
	xmem[_channelCombinationState + 1] = a0h			# LN: 159 | 
cline_159_0:			/* LN: 162 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x3)			# LN: 162 | 
	uhalfword(a0) = (_channelCombinationState + 2)			# LN: 162 | 
	xmem[i0] = a0			# LN: 162 | 
cline_162_0:			/* LN: 163 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x4)			# LN: 163 | 
	uhalfword(a0) = (_dynamic_loss + 0)			# LN: 163 | 
	xmem[i0] = a0			# LN: 163 | 
cline_163_0:			/* LN: 164 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x2)			# LN: 164 | 
	a0 = 0			# LN: 164 | 
	xmem[i0] = a0h			# LN: 164 | 
	do (0x3), label_end_93			# LN: 164 | 
cline_164_0:			/* LN: 166 | CYCLE: 0 | RULES: () */ 
label_begin_93:			/* LN: 164 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x3)			# LN: 166 | 
	i1 = i7 - (0x4)			# LN: 166 | 
	i1 = xmem[i1]			# LN: 166 | 
	i0 = xmem[i0]			# LN: 166 | 
	a0 = xmem[i1]			# LN: 166 | 
	xmem[i0] = a0h			# LN: 166 | 
	i0 = i7 - (0x3)			# LN: 166 | 
	i0 = xmem[i0]			# LN: 166 | 
	i1 = i7 - (0x3)			# LN: 166 | 
	i0 += 1			# LN: 166 | 
	xmem[i1] = i0			# LN: 166 | 
	i0 = i7 - (0x4)			# LN: 166 | 
	i0 = xmem[i0]			# LN: 166 | 
	i1 = i7 - (0x4)			# LN: 166 | 
	i0 += 1			# LN: 166 | 
	xmem[i1] = i0			# LN: 166 | 
cline_166_0:			/* LN: 164 | CYCLE: 0 | RULES: () */ 
init_latch_label_2:			/* LN: 167 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x2)			# LN: 164 | 
	a0 = xmem[i0]			# LN: 164 | 
	uhalfword(a1) = (0x1)			# LN: 164 | 
	a0 = a0 + a1			# LN: 164 | 
	i0 = i7 - (0x2)			# LN: 164 | 
label_end_93:			# LN: 164 | CYCLE: 5 | RULES: ()
	xmem[i0] = a0h			# LN: 164 | 
cline_164_1:			/* LN: 168 | CYCLE: 0 | RULES: () */ 
for_end_2:			/* LN: 164 | CYCLE: 0 | RULES: () */ 
	uhalfword(a0) = (0x3)			# LN: 168 | 
	xmem[_channelCombinationState + 5] = a0h			# LN: 168 | 
cline_168_0:			/* LN: 171 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x5)			# LN: 171 | 
	uhalfword(a0) = (_channelCombinationState + 6)			# LN: 171 | 
	xmem[i0] = a0			# LN: 171 | 
cline_171_0:			/* LN: 172 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x6)			# LN: 172 | 
	uhalfword(a0) = (_static_loss + 0)			# LN: 172 | 
	xmem[i0] = a0			# LN: 172 | 
cline_172_0:			/* LN: 173 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x2)			# LN: 173 | 
	a0 = 0			# LN: 173 | 
	xmem[i0] = a0h			# LN: 173 | 
	do (0x4), label_end_94			# LN: 173 | 
cline_173_0:			/* LN: 175 | CYCLE: 0 | RULES: () */ 
label_begin_94:			/* LN: 173 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x5)			# LN: 175 | 
	i1 = i7 - (0x6)			# LN: 175 | 
	i1 = xmem[i1]			# LN: 175 | 
	i0 = xmem[i0]			# LN: 175 | 
	a0 = xmem[i1]			# LN: 175 | 
	xmem[i0] = a0h			# LN: 175 | 
	i0 = i7 - (0x5)			# LN: 175 | 
	i0 = xmem[i0]			# LN: 175 | 
	i1 = i7 - (0x5)			# LN: 175 | 
	i0 += 1			# LN: 175 | 
	xmem[i1] = i0			# LN: 175 | 
	i0 = i7 - (0x6)			# LN: 175 | 
	i0 = xmem[i0]			# LN: 175 | 
	i1 = i7 - (0x6)			# LN: 175 | 
	i0 += 1			# LN: 175 | 
	xmem[i1] = i0			# LN: 175 | 
cline_175_0:			/* LN: 173 | CYCLE: 0 | RULES: () */ 
init_latch_label_3:			/* LN: 176 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x2)			# LN: 173 | 
	a0 = xmem[i0]			# LN: 173 | 
	uhalfword(a1) = (0x1)			# LN: 173 | 
	a0 = a0 + a1			# LN: 173 | 
	i0 = i7 - (0x2)			# LN: 173 | 
label_end_94:			# LN: 173 | CYCLE: 5 | RULES: ()
	xmem[i0] = a0h			# LN: 173 | 
cline_173_1:			/* LN: 177 | CYCLE: 0 | RULES: () */ 
for_end_3:			/* LN: 173 | CYCLE: 0 | RULES: () */ 
	uhalfword(a0) = (0x4)			# LN: 177 | 
	xmem[_channelCombinationState + 10] = a0h			# LN: 177 | 
cline_177_0:			/* LN: 179 | CYCLE: 0 | RULES: () */ 
	jmp (__epilogue_226)			# LN: 179 | 
__epilogue_226:			/* LN: 179 | CYCLE: 0 | RULES: () */ 
	i7 = i7 - (0x6)			# LN: 179 | 
	i7 -= 1			# LN: 179 | 
	ret			# LN: 179 | 



_fir_circular:			/* LN: 97 | CYCLE: 0 | RULES: () */ 
	xmem[i7] = i7			# LN: 97 | 
	i7 += 1			# LN: 97 | 
	i7 = i7 + (0x6)			# LN: 97 | 
	i4 = i7 - (0x1)			# LN: 97 | 
	xmem[i4] = a0h			# LN: 97 | 
	i4 = i7 - (0x2)			# LN: 97 | 
	xmem[i4] = i0			# LN: 97 | 
	i0 = i7 - (0x3)			# LN: 97 | 
	xmem[i0] = i1			# LN: 97 | 
cline_97_0:			/* LN: 103 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x3)			# LN: 103 | 
	i0 = xmem[i0]			# LN: 103 | 
	i1 = i7 - (0x4)			# LN: 103 | 
	a0 = xmem[i0]			# LN: 103 | 
	xmem[i1] = a0h			# LN: 103 | 
cline_103_0:			/* LN: 104 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x4)			# LN: 104 | 
	a0 = xmem[i0]			# LN: 104 | 
	i0 = i7 - (0x2)			# LN: 104 | 
	a1 = xmem[i0]			# LN: 104 | 
	a0 = a1 + a0			# LN: 104 | 
	AnyReg(i0, a0h)			# LN: 104 | 
	i1 = i7 - (0x1)			# LN: 104 | 
	a0 = xmem[i1]			# LN: 104 | 
	xmem[i0] = a0h			# LN: 104 | 
cline_104_0:			/* LN: 109 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x2)			# LN: 109 | 
	a0 = xmem[i0]			# LN: 109 | 
	AnyReg(i0, a0h)			# LN: 109 | 
	nm0 = (0x4001)			# LN: -1 | 
	i1 = i7 - (0x2)			# LN: 109 | 
	i0 += n			# LN: 109 | 
	nm0 = (0x0)			# LN: -1 | 
	xmem[i1] = i0			# LN: 109 | 
cline_109_0:			/* LN: 111 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x5)			# LN: 111 | 
	a0 = 0			# LN: 111 | 
	xmem[i0] = a0h			# LN: 111 | 
cline_111_0:			/* LN: 112 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x6)			# LN: 112 | 
	uhalfword(a0) = (0x1f)			# LN: 112 | 
	xmem[i0] = a0h			# LN: 112 | 
	do (0x20), label_end_98			# LN: 112 | 
cline_112_0:			/* LN: 114 | CYCLE: 0 | RULES: () */ 
label_begin_98:			/* LN: 112 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x6)			# LN: 114 | 
	a0 = xmem[i0]			# LN: 114 | 
	i0 = a0			# LN: 114 | 
	i1 = i7 - (0x4)			# LN: 114 | 
	i0 = i0 + (_coeffs + 0)			# LN: 114 | 
	a0 = xmem[i1]			# LN: 114 | 
	i1 = i7 - (0x2)			# LN: 114 | 
	a1 = xmem[i1]			# LN: 114 | 
	x0 = xmem[i0]; a0 = a1 + a0			# LN: 114, 114 | 
	AnyReg(i0, a0h)			# LN: 114 | 
	i1 = i7 - (0x5)			# LN: 114 | 
	a0 = xmem[i1]			# LN: 114 | 
	x1 = xmem[i0]			# LN: 114 | 
	a0 += x0 * x1			# LN: 114 | 
	i0 = i7 - (0x5)			# LN: 114 | 
	xmem[i0] = a0h			# LN: 114 | 
cline_114_0:			/* LN: 119 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x2)			# LN: 119 | 
	a0 = xmem[i0]			# LN: 119 | 
	AnyReg(i0, a0h)			# LN: 119 | 
	nm0 = (0x4001)			# LN: -1 | 
	i1 = i7 - (0x2)			# LN: 119 | 
	i0 += n			# LN: 119 | 
	nm0 = (0x0)			# LN: -1 | 
	xmem[i1] = i0			# LN: 119 | 
cline_119_0:			/* LN: 112 | CYCLE: 0 | RULES: () */ 
init_latch_label_0:			/* LN: 121 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x6)			# LN: 112 | 
	a0 = xmem[i0]			# LN: 112 | 
	uhalfword(a1) = (0x1)			# LN: 112 | 
	a0 = a0 - a1			# LN: 112 | 
	i0 = i7 - (0x6)			# LN: 112 | 
label_end_98:			# LN: 112 | CYCLE: 5 | RULES: ()
	xmem[i0] = a0h			# LN: 112 | 
cline_112_1:			/* LN: 123 | CYCLE: 0 | RULES: () */ 
for_end_0:			/* LN: 112 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x4)			# LN: 123 | 
	i1 = i7 - (0x3)			# LN: 123 | 
	i1 = xmem[i1]			# LN: 123 | 
	a0 = xmem[i0]			# LN: 123 | 
	xmem[i1] = a0h			# LN: 123 | 
cline_123_0:			/* LN: 125 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x5)			# LN: 125 | 
	a0 = xmem[i0]			# LN: 125 | 
	jmp (__epilogue_224)			# LN: 125 | 
cline_125_0:			/* LN: 126 | CYCLE: 0 | RULES: () */ 
__epilogue_224:			/* LN: 126 | CYCLE: 0 | RULES: () */ 
	i7 = i7 - (0x6)			# LN: 126 | 
	i7 -= 1			# LN: 126 | 
	ret			# LN: 126 | 



	# This construction should ensure linking of crt0 in case when target is a standalone program without the OS
	.if defined(_OVLY_)
		.if .strcmp('standalone',_OVLY_)=0
		.if .strcmp('crystal32',_TARGET_FAMILY_)=0
			.extern __start         # dummy use of __start to force linkage of crt0
dummy		.equ(__start)
		.else
			.extern __intvec         # dummy use of __intvec to force linkage of intvec
dummy		.equ(__intvec)
		.endif
		.endif
	.endif

_main:			/* LN: 334 | CYCLE: 0 | RULES: () */ 
	xmem[i7] = i7			# LN: 334 | 
	i7 += 1			# LN: 334 | 
	xmem[i7] = i2; i7 += 1			# LN: 334, 334 | 
	xmem[i7] = i3; i7 += 1			# LN: 334, 334 | 
	xmem[i7] = i6; i7 += 1			# LN: 334, 334 | 
	i7 = i7 + (0x212)			# LN: 334 | 
	i1 = i7 - (0x1)			# LN: 334 | 
	xmem[i1] = a0h			# LN: 334 | 
	i1 = i7 - (0x2)			# LN: 334 | 
	xmem[i1] = i0			# LN: 334 | 
cline_334_0:			/* LN: 352 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x3)			# LN: 352 | 
	a0 = 0			# LN: 352 | 
	xmem[i0] = a0h			# LN: 352 | 
	do (0x8), label_end_95			# LN: 352 | 
cline_352_0:			/* LN: 353 | CYCLE: 0 | RULES: () */ 
label_begin_95:			/* LN: 352 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x3)			# LN: 353 | 
	a1 = xmem[i0]; a0 = 0			# LN: 353, 353 | 
	a1 = a1 << 4			# LN: 353 | 
	i0 = a1			# LN: 353 | 
	uhalfword(a1) = (0x10)			# LN: 353 | 
	i0 = i0 + (_sampleBuffer + 0)			# LN: 353 | 
	call (_memset)			# LN: 353 | 
cline_353_0:			/* LN: 352 | CYCLE: 0 | RULES: () */ 
init_latch_label_4:			/* LN: 353 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x3)			# LN: 352 | 
	a0 = xmem[i0]			# LN: 352 | 
	uhalfword(a1) = (0x1)			# LN: 352 | 
	a0 = a0 + a1			# LN: 352 | 
	i0 = i7 - (0x3)			# LN: 352 | 
label_end_95:			# LN: 352 | CYCLE: 5 | RULES: ()
	xmem[i0] = a0h			# LN: 352 | 
cline_352_1:			/* LN: 357 | CYCLE: 0 | RULES: () */ 
for_end_4:			/* LN: 352 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x2)			# LN: 357 | 
	i0 = xmem[i0]			# LN: 357 | 
	i1 = i7 - (259 - 0)			# LN: 357 | 
	i4 = xmem[i0]			# LN: 357 | 
	i0 = i1			# LN: 357 | 
	i1 = i4			# LN: 357 | 
	call (_strcpy)			# LN: 357 | 
cline_357_0:			/* LN: 358 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (259 - 0)			# LN: 358 | 
	call (_cl_wavread_open)			# LN: 358 | 
	AnyReg(i0, a0h)			# LN: 358 | 
	i1 = i7 - (0x104)			# LN: 358 | 
	xmem[i1] = i0			# LN: 358 | 
cline_358_0:			/* LN: 359 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x104)			# LN: 359 | 
	a0 = xmem[i0]			# LN: 359 | 
	a0 & a0			# LN: 359 | 
	if (a != 0) jmp (else_0)			# LN: 359 | 
cline_359_0:			/* LN: 361 | CYCLE: 0 | RULES: () */ 
	i0 = (0) + (_string_const_0)			# LN: 361 | 
	call (_printf)			# LN: 361 | 
cline_361_0:			/* LN: 362 | CYCLE: 0 | RULES: () */ 
	halfword(a0) = (0xffff)			# LN: 362 | 
	jmp (__epilogue_228)			# LN: 362 | 
cline_362_0:			/* LN: 369 | CYCLE: 0 | RULES: () */ 
endif_0:			/* LN: 359 | CYCLE: 0 | RULES: () */ 
else_0:			/* LN: 359 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x105)			# LN: 369 | 
	uhalfword(a0) = (0x6)			# LN: 369 | 
	xmem[i0] = a0h			# LN: 369 | 
cline_369_0:			/* LN: 370 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x104)			# LN: 370 | 
	i0 = xmem[i0]			# LN: 370 | 
	call (_cl_wavread_bits_per_sample)			# LN: 370 | 
	i0 = i7 - (0x106)			# LN: 370 | 
	xmem[i0] = a0h			# LN: 370 | 
cline_370_0:			/* LN: 371 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x104)			# LN: 371 | 
	i0 = xmem[i0]			# LN: 371 | 
	call (_cl_wavread_frame_rate)			# LN: 371 | 
	i0 = i7 - (0x107)			# LN: 371 | 
	xmem[i0] = a0h			# LN: 371 | 
cline_371_0:			/* LN: 372 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x104)			# LN: 372 | 
	i0 = xmem[i0]			# LN: 372 | 
	call (_cl_wavread_number_of_frames)			# LN: 372 | 
	i0 = i7 - (0x108)			# LN: 372 | 
	xmem[i0] = a0h			# LN: 372 | 
cline_372_0:			/* LN: 377 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x2)			# LN: 377 | 
	i0 = xmem[i0]			# LN: 377 | 
	i1 = i7 - (520 - 0)			# LN: 377 | 
	i0 += 1			# LN: 377 | 
	i4 = xmem[i0]			# LN: 377 | 
	i0 = i1			# LN: 377 | 
	i1 = i4			# LN: 377 | 
	call (_strcpy)			# LN: 377 | 
cline_377_0:			/* LN: 378 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (520 - 0)			# LN: 378 | 
	i1 = i7 - (0x106)			# LN: 378 | 
	a0 = xmem[i1]			# LN: 378 | 
	i1 = i7 - (0x105)			# LN: 378 | 
	a1 = xmem[i1]			# LN: 378 | 
	i1 = i7 - (0x107)			# LN: 378 | 
	b0 = xmem[i1]			# LN: 378 | 
	call (_cl_wavwrite_open)			# LN: 378 | 
	AnyReg(i0, a0h)			# LN: 378 | 
	i1 = i7 - (0x209)			# LN: 378 | 
	xmem[i1] = i0			# LN: 378 | 
cline_378_0:			/* LN: 379 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x209)			# LN: 379 | 
	a0 = xmem[i0]			# LN: 379 | 
	a0 & a0			# LN: 379 | 
	if (a != 0) jmp (else_1)			# LN: 379 | 
cline_379_0:			/* LN: 381 | CYCLE: 0 | RULES: () */ 
	i0 = (0) + (_string_const_1)			# LN: 381 | 
	call (_printf)			# LN: 381 | 
cline_381_0:			/* LN: 382 | CYCLE: 0 | RULES: () */ 
	halfword(a0) = (0xffff)			# LN: 382 | 
	jmp (__epilogue_228)			# LN: 382 | 
cline_382_0:			/* LN: 403 | CYCLE: 0 | RULES: () */ 
endif_1:			/* LN: 379 | CYCLE: 0 | RULES: () */ 
else_1:			/* LN: 379 | CYCLE: 0 | RULES: () */ 
	a0 = xmem[__extractedConst_0_3 + 0]			# LN: 403 | 
	xmem[_inputLeftLoss + 0] = a0h			# LN: 403 | 
cline_403_0:			/* LN: 404 | CYCLE: 0 | RULES: () */ 
	a0 = xmem[__extractedConst_0_3 + 0]			# LN: 404 | 
	xmem[_inputRightLoss + 0] = a0h			# LN: 404 | 
cline_404_0:			/* LN: 405 | CYCLE: 0 | RULES: () */ 
	a0 = xmem[__extractedConst_0_3 + 0]			# LN: 405 | 
	xmem[_inputHeadroomLoss + 0] = a0h			# LN: 405 | 
cline_405_0:			/* LN: 406 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20a)			# LN: 406 | 
	uhalfword(a0) = (_dynamic_loss + 0)			# LN: 406 | 
	xmem[i0] = a0			# LN: 406 | 
cline_406_0:			/* LN: 407 | CYCLE: 0 | RULES: () */ 
	a0 = xmem[_inputLeftLoss + 0]			# LN: 407 | 
	xmem[_dynamic_loss + 0] = a0h			# LN: 407 | 
	i0 = i7 - (0x20a)			# LN: 407 | 
	uhalfword(a0) = (_dynamic_loss + 1)			# LN: 407 | 
	xmem[i0] = a0			# LN: 407 | 
cline_407_0:			/* LN: 408 | CYCLE: 0 | RULES: () */ 
	a0 = xmem[_inputRightLoss + 0]			# LN: 408 | 
	xmem[_dynamic_loss + 1] = a0h			# LN: 408 | 
	i0 = i7 - (0x20a)			# LN: 408 | 
	uhalfword(a0) = (_dynamic_loss + 2)			# LN: 408 | 
	xmem[i0] = a0			# LN: 408 | 
cline_408_0:			/* LN: 409 | CYCLE: 0 | RULES: () */ 
	a0 = xmem[_inputHeadroomLoss + 0]			# LN: 409 | 
	xmem[_dynamic_loss + 2] = a0h			# LN: 409 | 
cline_409_0:			/* LN: 412 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x2)			# LN: 412 | 
	i0 = xmem[i0]			# LN: 412 | 
	nop #empty cycle
	i0 = i0 + (0x5)			# LN: 412 | 
	i0 = xmem[i0]			# LN: 412 | 
	a0 = i0			# LN: 412 | 
	xmem[_outputMode + 0] = a0h			# LN: 412 | 
cline_412_0:			/* LN: 418 | CYCLE: 0 | RULES: () */ 
	call (_channel_combination_state_init)			# LN: 418 | 
cline_418_0:			/* LN: 429 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x108)			# LN: 429 | 
	a0 = xmem[i0]			# LN: 429 | 
	uhalfword(a1) = (0x10)			# LN: 429 | 
	call (__div)			# LN: 429 | 
	i0 = i7 - (0x20b)			# LN: 429 | 
	xmem[i0] = a0h			# LN: 429 | 
cline_429_0:			/* LN: 432 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20c)			# LN: 432 | 
	a0 = 0			# LN: 432 | 
	xmem[i0] = a0h			# LN: 432 | 
for_5:			/* LN: 432 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20c)			# LN: 432 | 
	a0 = xmem[i0]			# LN: 432 | 
	i0 = i7 - (0x20b)			# LN: 432 | 
	a1 = xmem[i0]			# LN: 432 | 
	a0 - a1			# LN: 432 | 
	if (a >= 0) jmp (for_end_5)			# LN: 432 | 
cline_432_0:			/* LN: 434 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20d)			# LN: 434 | 
	a0 = 0			# LN: 434 | 
	xmem[i0] = a0h			# LN: 434 | 
	do (0x10), label_end_96			# LN: 434 | 
cline_434_0:			/* LN: 436 | CYCLE: 0 | RULES: () */ 
label_begin_96:			/* LN: 434 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20e)			# LN: 436 | 
	a0 = 0			# LN: 436 | 
	xmem[i0] = a0h			# LN: 436 | 
for_7:			/* LN: 436 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20e)			# LN: 436 | 
	a0 = xmem[i0]			# LN: 436 | 
	i0 = i7 - (0x105)			# LN: 436 | 
	a1 = xmem[i0]			# LN: 436 | 
	a0 - a1			# LN: 436 | 
	if (a >= 0) jmp (for_end_7)			# LN: 436 | 
cline_436_0:			/* LN: 438 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x104)			# LN: 438 | 
	i0 = xmem[i0]			# LN: 438 | 
	call (_cl_wavread_recvsample)			# LN: 438 | 
	i0 = i7 - (0x20f)			# LN: 438 | 
	xmem[i0] = a0h			# LN: 438 | 
cline_438_0:			/* LN: 439 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20e)			# LN: 439 | 
	a0 = xmem[i0]			# LN: 439 | 
	a0 = a0 << 4			# LN: 439 | 
	i0 = a0			# LN: 439 | 
	i1 = i7 - (0x20d)			# LN: 439 | 
	i0 = i0 + (_sampleBuffer + 0)			# LN: 439 | 
	a0 = xmem[i1]			# LN: 439 | 
	a1 = i0			# LN: 439 | 
	a0 = a1 + a0			# LN: 439 | 
	AnyReg(i0, a0h)			# LN: 439 | 
	i1 = i7 - (0x20f)			# LN: 439 | 
	a0 = xmem[i1]			# LN: 439 | 
	ymem[i0] = a0h			# LN: 439 | 
cline_439_0:			/* LN: 436 | CYCLE: 0 | RULES: () */ 
init_latch_label_5:			/* LN: 440 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20e)			# LN: 436 | 
	a0 = xmem[i0]			# LN: 436 | 
	uhalfword(a1) = (0x1)			# LN: 436 | 
	a0 = a0 + a1			# LN: 436 | 
	i0 = i7 - (0x20e)			# LN: 436 | 
	xmem[i0] = a0h			# LN: 436 | 
	jmp (for_7)			# LN: 436 | 
cline_436_1:			/* LN: 434 | CYCLE: 0 | RULES: () */ 
init_latch_label_6:			/* LN: 441 | CYCLE: 0 | RULES: () */ 
for_end_7:			/* LN: 436 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20d)			# LN: 434 | 
	a0 = xmem[i0]			# LN: 434 | 
	uhalfword(a1) = (0x1)			# LN: 434 | 
	a0 = a0 + a1			# LN: 434 | 
	i0 = i7 - (0x20d)			# LN: 434 | 
label_end_96:			# LN: 434 | CYCLE: 5 | RULES: ()
	xmem[i0] = a0h			# LN: 434 | 
cline_434_1:			/* LN: 451 | CYCLE: 0 | RULES: () */ 
for_end_6:			/* LN: 434 | CYCLE: 0 | RULES: () */ 
	i0 = (0) + (_sampleBuffer + 0)			# LN: 451 | 
	i1 = (0) + (_sampleBuffer + 0)			# LN: 451 | 
	i4 = (0) + (_sampleBuffer + 16)			# LN: 451 | 
	i5 = (0) + (_sampleBuffer + 32)			# LN: 451 | 
	i2 = (0) + (_sampleBuffer + 32)			# LN: 451 | 
	i3 = (0) + (_sampleBuffer + 48)			# LN: 451 | 
	i6 = i7 - (0x212)			# LN: 451 | 
	xmem[i6] = i3			# LN: 451 | 
	i3 = (0) + (_sampleBuffer + 64)			# LN: 451 | 
	i6 = i7 - (0x211)			# LN: 451 | 
	xmem[i6] = i3			# LN: 451 | 
	i3 = (0) + (_sampleBuffer + 80)			# LN: 451 | 
	i6 = i7 - (0x210)			# LN: 451 | 
	xmem[i6] = i3			# LN: 451 | 
	i3 = i7 + (0x3)			# LN: 451 | 
	xmem[i3] = i2			# LN: 451 | 
	i2 = i7 + (0x2)			# LN: 451 | 
	i3 = i7 - (0x212)			# LN: 451 | 
	i3 = xmem[i3]			# LN: 451 | 
	xmem[i2] = i3			# LN: 451 | 
	i2 = i7 + (0x1)			# LN: 451 | 
	i3 = i7 - (0x211)			# LN: 451 | 
	i3 = xmem[i3]			# LN: 451 | 
	xmem[i2] = i3			# LN: 451 | 
	i2 = i7 - (0x0)			# LN: 451 | 
	i3 = i7 - (0x210)			# LN: 451 | 
	i3 = xmem[i3]			# LN: 451 | 
	xmem[i2] = i3			# LN: 451 | 
	i7 = i7 + (0x4)			# LN: 451 | 
	call (_channel_combination)			# LN: 451 | 
	i7 = i7 - (0x4)			# LN: 451 | 
cline_451_0:			/* LN: 454 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20d)			# LN: 454 | 
	a0 = 0			# LN: 454 | 
	xmem[i0] = a0h			# LN: 454 | 
	do (0x10), label_end_97			# LN: 454 | 
cline_454_0:			/* LN: 456 | CYCLE: 0 | RULES: () */ 
label_begin_97:			/* LN: 454 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20e)			# LN: 456 | 
	a0 = 0			# LN: 456 | 
	xmem[i0] = a0h			# LN: 456 | 
for_9:			/* LN: 456 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20e)			# LN: 456 | 
	a0 = xmem[i0]			# LN: 456 | 
	i0 = i7 - (0x105)			# LN: 456 | 
	a1 = xmem[i0]			# LN: 456 | 
	a0 - a1			# LN: 456 | 
	if (a >= 0) jmp (for_end_9)			# LN: 456 | 
cline_456_0:			/* LN: 458 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20e)			# LN: 458 | 
	a0 = xmem[i0]			# LN: 458 | 
	a0 = a0 << 4			# LN: 458 | 
	i0 = a0			# LN: 458 | 
	i1 = i7 - (0x20d)			# LN: 458 | 
	i0 = i0 + (_sampleBuffer + 0)			# LN: 458 | 
	a0 = xmem[i1]			# LN: 458 | 
	a1 = i0			# LN: 458 | 
	a0 = a1 + a0			# LN: 458 | 
	AnyReg(i0, a0h)			# LN: 458 | 
	i1 = i7 - (0x20f)			# LN: 458 | 
	a0 = ymem[i0]			# LN: 458 | 
	xmem[i1] = a0h			# LN: 458 | 
cline_458_0:			/* LN: 459 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x209)			# LN: 459 | 
	i1 = i7 - (0x20f)			# LN: 459 | 
	a0 = xmem[i1]			# LN: 459 | 
	i0 = xmem[i0]			# LN: 459 | 
	call (_cl_wavwrite_sendsample)			# LN: 459 | 
cline_459_0:			/* LN: 456 | CYCLE: 0 | RULES: () */ 
init_latch_label_7:			/* LN: 460 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20e)			# LN: 456 | 
	a0 = xmem[i0]			# LN: 456 | 
	uhalfword(a1) = (0x1)			# LN: 456 | 
	a0 = a0 + a1			# LN: 456 | 
	i0 = i7 - (0x20e)			# LN: 456 | 
	xmem[i0] = a0h			# LN: 456 | 
	jmp (for_9)			# LN: 456 | 
cline_456_1:			/* LN: 454 | CYCLE: 0 | RULES: () */ 
init_latch_label_8:			/* LN: 461 | CYCLE: 0 | RULES: () */ 
for_end_9:			/* LN: 456 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20d)			# LN: 454 | 
	a0 = xmem[i0]			# LN: 454 | 
	uhalfword(a1) = (0x1)			# LN: 454 | 
	a0 = a0 + a1			# LN: 454 | 
	i0 = i7 - (0x20d)			# LN: 454 | 
label_end_97:			# LN: 454 | CYCLE: 5 | RULES: ()
	xmem[i0] = a0h			# LN: 454 | 
cline_454_1:			/* LN: 432 | CYCLE: 0 | RULES: () */ 
init_latch_label_9:			/* LN: 462 | CYCLE: 0 | RULES: () */ 
for_end_8:			/* LN: 454 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x20c)			# LN: 432 | 
	a0 = xmem[i0]			# LN: 432 | 
	uhalfword(a1) = (0x1)			# LN: 432 | 
	a0 = a0 + a1			# LN: 432 | 
	i0 = i7 - (0x20c)			# LN: 432 | 
	xmem[i0] = a0h			# LN: 432 | 
	jmp (for_5)			# LN: 432 | 
cline_432_1:			/* LN: 467 | CYCLE: 0 | RULES: () */ 
for_end_5:			/* LN: 432 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x104)			# LN: 467 | 
	i0 = xmem[i0]			# LN: 467 | 
	call (_cl_wavread_close)			# LN: 467 | 
cline_467_0:			/* LN: 468 | CYCLE: 0 | RULES: () */ 
	i0 = i7 - (0x209)			# LN: 468 | 
	i0 = xmem[i0]			# LN: 468 | 
	call (_cl_wavwrite_close)			# LN: 468 | 
cline_468_0:			/* LN: 471 | CYCLE: 0 | RULES: () */ 
	a0 = 0			# LN: 471 | 
	jmp (__epilogue_228)			# LN: 471 | 
cline_471_0:			/* LN: 472 | CYCLE: 0 | RULES: () */ 
__epilogue_228:			/* LN: 472 | CYCLE: 0 | RULES: () */ 
	i7 = i7 - (0x212)			# LN: 472 | 
	i7 -= 1			# LN: 472 | 
	i6 = xmem[i7]; i7 -= 1			# LN: 472, 472 | 
	i3 = xmem[i7]; i7 -= 1			# LN: 472, 472 | 
	i2 = xmem[i7]; i7 -= 1			# LN: 472, 472 | 
	ret			# LN: 472 | 
