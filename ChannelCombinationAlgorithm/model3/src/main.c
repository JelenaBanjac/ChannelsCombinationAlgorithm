/////////////////////////////////////////////////////////////////////////////////
// @file main.cpp
//
// Module: multitapEchoDSP
// Description:  Add multitap echo to input signal
// $Source: $
// $Revision: 1.0 $
// $Date: <date> $
// $Author: <student name> $
//
/////////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <dsplib\wavefile.h>
#include <stdfix.h>
#include <string.h>
#include <common.h>
#include <circbuff.h>
#include <math.h>
#include <circbuff.h>
#include <channelCombination.h>
/////////////////////////////////////////////////////////////////////////////////
// IO buffers
/////////////////////////////////////////////////////////////////////////////////
__memY DSPfract sampleBuffer[MAX_NUM_CHANNEL][BLOCK_SIZE];
__memX DSPfract  output[OUTPUTS_NUM];
/////////////////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////////////////////////////
// Output Mode
/////////////////////////////////////////////////////////////////////////////////
typedef enum
{
	OUTPUT_MODE_3_2_0 = 0,	//LR, C, LsRs
	OUTPUT_MODE_2_0_0,		//LR
	OUTPUT_MODE_2_0_1,		//LR, LFE
	OUTPUT_MODE_3_2_1		//LR c, LsRs, LFE
} OutputMode;

/////////////////////////////////////////////////////////////////////////////////
// Control state structure
/////////////////////////////////////////////////////////////////////////////////
typedef struct
{
	__memX DSPfract * pChannelCombinationBuff;
	DSPint bufferLength;
	DSPfract dynamic_loss[N_DYNAMIC_LOSS];
	DSPint n_dynamic_loss;
	DSPfract  static_loss[N_STATIC_LOSS];
	DSPint n_static_loss;
} ChannelCombinationState;

typedef struct
{
	__memY DSPfract * pInBufLeft;		// System inputs
	__memY DSPfract * pInBufRight;
	__memY DSPfract * pOutBufLs;		// System outputs
	__memY DSPfract * pOutBufL;
	__memY DSPfract * pOutBufC;
	__memY DSPfract * pOutBufR;
	__memY DSPfract * pOutBufRs;
	__memY DSPfract * pOutBufLFE;
	DSPint  bufferLength;		// Lenght of these samples in inputn (and output as well)
} ChannelCombination;

DSPfract  history;
DSPint  p_state;
OutputMode outputMode;
DSPfract  inputLeftLoss;
DSPfract  inputRightLoss;
DSPfract  inputHeadroomLoss;

/////////////////////////////////////////////////////////////////////////////////
// Channel combination loss state and initialization constants
/////////////////////////////////////////////////////////////////////////////////
ChannelCombinationState channelCombinationState;
ChannelCombination channelCombination;
__memX DSPfract __attribute__((__aligned__(32))) channel_combination_buffer[BUFFER_MAX_LENGTH];
__memX DSPfract dynamic_loss[N_DYNAMIC_LOSS];					// Dynamic loss/gain values
__memX const DSPfract static_loss[N_STATIC_LOSS] = {FRACT_NUM(0.7943282347242815), FRACT_NUM(0.7079457843841379), FRACT_NUM(0.6309573444801932), FRACT_NUM(0.8912509381337456)};
__memX const DSPfract coeffs[N_COEFFS] = {FRACT_NUM(0.000084), FRACT_NUM(0.000163), FRACT_NUM(0.000269), FRACT_NUM(0.000404), FRACT_NUM(0.000568), FRACT_NUM(0.000763), FRACT_NUM(0.000986), FRACT_NUM(0.001236), FRACT_NUM(0.001509), FRACT_NUM(0.001800),FRACT_NUM(0.002103), FRACT_NUM(0.002411), FRACT_NUM(0.002717), FRACT_NUM(0.003012), FRACT_NUM(0.003289), FRACT_NUM(0.003539), FRACT_NUM(0.003756), FRACT_NUM(0.003931), FRACT_NUM(0.004061), FRACT_NUM(0.004140),FRACT_NUM(0.004167), FRACT_NUM(0.004140), FRACT_NUM(0.004061), FRACT_NUM(0.003931), FRACT_NUM(0.003756), FRACT_NUM(0.003539), FRACT_NUM(0.003289), FRACT_NUM(0.003012), FRACT_NUM(0.002717), FRACT_NUM(0.002411),FRACT_NUM(0.002103), FRACT_NUM(0.001800)};//, FRACT_NUM(0.001509), FRACT_NUM(0.001236), FRACT_NUM(0.000986), FRACT_NUM(0.000763), FRACT_NUM(0.000568), FRACT_NUM(0.000404), FRACT_NUM(0.000269), FRACT_NUM(0.000163),FRACT_NUM(0.000084)};
//__memX const DSPfract coeffs[N_COEFFS] = {0.000084, 0.000163, 0.000269, 0.000404, 0.000568, 0.000763, 0.000986, 0.001236, 0.001509, 0.001800,0.002103, 0.002411, 0.002717, 0.003012, 0.003289, 0.003539,0.003756, 0.003931, 0.004061, 0.004140,0.004167, 0.004140, 0.004061, 0.003931, 0.003756, 0.003539, 0.003289, 0.003012, 0.002717, 0.002411,0.002103, 0.001800, 0.001509, 0.001236, 0.000986, 0.000763, 0.000568, 0.000404, 0.000269, 0.000163,0.000084};


/////////////////////////////////////////////////////////////////////////////////
// Multitap delay state and initialization constants
/////////////////////////////////////////////////////////////////////////////////
/*EchoState echoState;
DSPfract echo_buffer[ECHO_MAX_LENGTH];
const DSPint initial_delay[N_TAP] = {1024, 1536, 2560, 3072};
const DSPfract initial_gain[N_TAP] = {FRACT_NUM(0.25), FRACT_NUM(0.125), FRACT_NUM(0.0625), FRACT_NUM(0.0625)};
const DSPfract initial_input_gain = FRACT_NUM(0.5);*/
/////////////////////////////////////////////////////////////////////////////////

DSPfract fir_circular(DSPfract input, __memX DSPfract *history, __memX DSPint *p_state)
{
	DSPint i;
	DSPint state;
	DSPfract ret_val;

	state = *p_state;
	history[state] = input;
	/*if (++state >= N_COEFFS)
	{
		state = 0;
	}*/
	history = CIRC_INC(history, MOD_32 + 1);

	ret_val = FRACT_NUM(0.0);
	for (i = N_COEFFS - 1; i >= 0; i --)
	{
		ret_val += coeffs[i] * history[state];
		/*if (++state >= N_COEFFS)
		{
			state = 0;
		}*/
		history = CIRC_INC(history, MOD_32 + 1);

	}

	*p_state = state;

	return ret_val;
}

/////////////////////////////////////////////////////////////////////////////////
// @Author	Jelena Banjac
// @Date	3.10.2017
//
// Function:
// multitap_echo_init
//
// @param - echoState - Control state structure
//		  - buffer - buffer for keeping delayed samples
//		  - echoBufLen - Length of buffer
//		  - delay - array containing delay values in number of samples
//		  - input_gain - gain to be applayed to input sample
//		  - tap_gain - array of gains to be applayed to each delayed sample
//		  - n_tap - number of taps (equals length of delay and tap_gain)
//
// @return - nothing
// Comment: Initialize echoState structure
//
// E-mail:	<email>
//
/////////////////////////////////////////////////////////////////////////////////
void channel_combination_state_init()
{
	DSPint i;
	__memX DSPfract *pInitChannelCombinationBuffer = channel_combination_buffer;
	for (i=0; i<BUFFER_MAX_LENGTH; i++)
	{
		*pInitChannelCombinationBuffer = FRACT_NUM(0.0);
		pInitChannelCombinationBuffer++;
	}
	channelCombinationState.pChannelCombinationBuff = channel_combination_buffer;
	channelCombinationState.bufferLength = BUFFER_MAX_LENGTH;

	{
	__memX DSPfract *pChannelCombinationDynamicLoss = channelCombinationState.dynamic_loss;
	__memX DSPfract *pInitChannelCombinationDynamicLoss = dynamic_loss;
	for (i=0; i<N_DYNAMIC_LOSS; i++)
	{
		*(pChannelCombinationDynamicLoss++) = *(pInitChannelCombinationDynamicLoss++);
	}
	channelCombinationState.n_dynamic_loss = N_DYNAMIC_LOSS;
	}
	{
	__memX DSPfract *pChannelCombinationStaticLoss = channelCombinationState.static_loss;
	__memX const DSPfract *pInitChannelCombinationStaticLoss = static_loss;
	for (i=0; i<N_STATIC_LOSS; i++)
	{
		*(pChannelCombinationStaticLoss++) = *(pInitChannelCombinationStaticLoss++);
	}
	channelCombinationState.n_static_loss = N_STATIC_LOSS;
	}
}

//void channel_combination_init(ChannelCombination* channelCombination, double* inBufLeft, double* outBufC, double* inBufRight, double* outBufLs, double* outBufRs, double* outBufLFE, int buffLen)
/*void channel_combination_init()
{
	channelCombination.pInBufLeft = sampleBuffer[0];
	channelCombination.pInBufRight = sampleBuffer[2];
	channelCombination.pOutBufLs = sampleBuffer[3];
	channelCombination.pOutBufL = sampleBuffer[0];
	channelCombination.pOutBufC = sampleBuffer[1];
	channelCombination.pOutBufR = sampleBuffer[2];
	channelCombination.pOutBufRs = sampleBuffer[4];
	channelCombination.pOutBufLFE = sampleBuffer[5];
	channelCombination.bufferLength = BLOCK_SIZE;
}*/


/////////////////////////////////////////////////////////////////////////////////
// @Author	Jelena Banjac
// @Date	3.10.2017
//
// Function:
// multitap_echo
//
// @param - pInbuf - Buffer with input samples
//		  - pOutbuf - Buffer with output samples
//		  - inputLen - Length of input and output buffer
//		  - echoState - Control state structure
//
// @return - nothing
// Comment: Apply echo to input samples
//
// E-mail:	<email>
//
/////////////////////////////////////////////////////////////////////////////////
/*void channel_combination(__memY DSPfract* inBufLeft, __memY DSPfract* outBufL, __memY DSPfract* outBufC, __memY DSPfract* inBufRight,
		__memY DSPfract* outBufR, __memY DSPfract* outBufLs, __memY DSPfract* outBufRs, __memY DSPfract* outBufLFE)
{
	int i;
	__memX DSPfract *out = output;
	__memX DSPfract *pDynamicLoss = channelCombinationState.dynamic_loss;
	__memX DSPfract *pStaticLoss = channelCombinationState.static_loss;
	//__memY DSPfract *pInBufLeft = channelCombination.pInBufLeft;
	//__memY DSPfract *pInBufRight = channelCombination.pInBufRight;
	//__memY DSPfract *pOutBufLs = channelCombination.pOutBufLs;
	//__memY DSPfract *pOutBufL = channelCombination.pOutBufL;
	//__memY DSPfract *pOutBufC = channelCombination.pOutBufC;
	//__memY DSPfract *pOutBufR = channelCombination.pOutBufR;
	//__memY DSPfract *pOutBufRs = channelCombination.pOutBufRs;
	//__memY DSPfract *pOutBufLFE = channelCombination.pOutBufLFE;
	__memY DSPfract *pInBufLeft = inBufLeft;
	__memY DSPfract *pInBufRight = inBufRight;
	__memY DSPfract *pOutBufLs = outBufLs;
	__memY DSPfract *pOutBufL = outBufL;
	__memY DSPfract *pOutBufC = outBufC;
	__memY DSPfract *pOutBufR = outBufR;
	__memY DSPfract *pOutBufRs = outBufRs;
	__memY DSPfract *pOutBufLFE = outBufLFE;

	//double *pInBufLeft = sampleBuffer[0];
	//double *pInBufRight = sampleBuffer[2];
	//double *pOutBufLs = sampleBuffer[3];
	//double *pOutBufL = sampleBuffer[0];
	//double *pOutBufC = sampleBuffer[1];
	//double *pOutBufR = sampleBuffer[2];
	//double *pOutBufRs = sampleBuffer[4];
	//double *pOutBufLFE = sampleBuffer[5];

	for(i = 0; i < BLOCK_SIZE; i++)
	{
		*(out++) = (*(pInBufLeft++)) * (*(pDynamicLoss++));			// state 0
		*(out++) = (*(pInBufRight++)) * (*(pDynamicLoss++));		// state 1
		*(out++) = *(out-2) + *(out-1);						// state 2
		*(out++) = *(out-1) * (*pDynamicLoss);				// state 3

		switch (outputMode)
		{
		case OUTPUT_MODE_3_2_0:		// L R C Ls Rs
		case OUTPUT_MODE_3_2_1:		// L R C Ls Rs LFE
			*(out++) = *(out-4) * (*(pStaticLoss++));		// state 4
			*(out++) = *(out-2) * (*(pStaticLoss++));		// state 5
			*(out++) = *(out-3) * (*(pStaticLoss++));		// state 6
			*(out++) = *(out-6) * (*(pStaticLoss));			// state 7
			*(out++) = *(out-4) + *(out-3);				// state 8
			*out = *(out-3) + *(out-2);					// state 9

			out -= 9;	// Restart
			pStaticLoss -= 3;
			pDynamicLoss -= 2;

			*(pOutBufLs++) = *(out+8);	// Ls buffer output
			*(pOutBufL++) = *(out+5);	// L buffer output
			*pOutBufC = *(out+3);	// C buffer output
			*(pOutBufR++) = *(out+6);	// R buffer output
			*(pOutBufRs++) = *(out+9);	// Rs buffer output

			if (OUTPUT_MODE_3_2_1)
			{
				*(pOutBufLFE++) = fir_circular(*pOutBufC, &history, &p_state);	// LFE buffer output
				*(pOutBufC++);
			}
			else
			{
				*(pOutBufC++);
				*(pOutBufLFE++) = FRACT_NUM(0.0);	// LFE buffer output
			}
			break;
		case OUTPUT_MODE_2_0_0:		// L R
		case OUTPUT_MODE_2_0_1:		// L R LFE
			*(out++) = *(out-1) * (*(pStaticLoss+1));		// state 5 (in place of state 4)
			*(out) = *(out-2) * (*(pStaticLoss+2));		// state 6 (in place of state 5)

			out -= 5;	// Restart
			pDynamicLoss -= 2;

			*(pOutBufLs++) = FRACT_NUM(0.0);		// Ls buffer output
			*(pOutBufL++) = *(out+4);	// L buffer output
			*(pOutBufR++) = *(out+5);	// R buffer output
			*(pOutBufRs++) = FRACT_NUM(0.0);		// Rs buffer output

			if (OUTPUT_MODE_2_0_1)
			{
				*pOutBufC = *(out+3);	// C buffer output
				*(pOutBufLFE++) = fir_circular(*pOutBufC, &history, &p_state); // LFE buffer output
				*(pOutBufC++);
			}
			else
			{
				*(pOutBufC++) = FRACT_NUM(0.0);		// C buffer output
				*(pOutBufLFE++) = FRACT_NUM(0.0);	// LFE buffer output
			}
			break;
		}

	}
	//p_state = 0;
	//pInBufLeft = pInBufLeft-16;
	//pInBufRight = pInBufRight-16;
}*/

/////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////
// @Author    <student name>
// @Date        <date>
//
// Function:
// main
//
// @param - nothing
// @return - nothing
// Comment: main routine of a program
//
/////////////////////////////////////////////////////////////////////////////////

int main(DSPint argc, char *argv[])
 {
    WAVREAD_HANDLE *wav_in;
    WAVWRITE_HANDLE *wav_out;

	char WavInputName[256];
	char WavOutputName[256];

    int nChannels;
	int bitsPerSample;
    int sampleRate;
    int iNumSamples;
    int l;
    //int it;
    //int num[N_DYNAMIC_LOSS];
    __memX DSPfract *pDynamicLoss;

    // Init channel buffers
	for(l=0; l<MAX_NUM_CHANNEL; l++)
		memset(&sampleBuffer[l],0,BLOCK_SIZE);

	// Open input wav file
	//-------------------------------------------------
	strcpy(WavInputName,argv[0]);
	wav_in = cl_wavread_open(WavInputName);
	if(wav_in == NULL)
    {
        printf("Error: Could not open input wavefile.\n");
        return -1;
    }
	//-------------------------------------------------

	// Read input wav header
	//-------------------------------------------------
	//nChannels = cl_wavread_getnchannels(wav_in);
	nChannels = 6;
    bitsPerSample = cl_wavread_bits_per_sample(wav_in);
    sampleRate = cl_wavread_frame_rate(wav_in);
    iNumSamples =  cl_wavread_number_of_frames(wav_in);
	//-------------------------------------------------

	// Open output wav file
	//-------------------------------------------------
	strcpy(WavOutputName,argv[1]);
	wav_out = cl_wavwrite_open(WavOutputName, bitsPerSample, nChannels, sampleRate);
	if(!wav_out)
    {
        printf("Error: Could not open output wavefile.\n");
        return -1;
    }
	//-------------------------------------------------

	// Dynamic loss/gain values

	/*sscanf (argv[3],"%d",&num[1]);
	sscanf (argv[4],"%d",&num[2]);
	sscanf (argv[5],"%d",&num[3]);*/


	{
		/*DSPfract *pDynamicLoss = dynamic_loss;
		for (it=0; it<N_DYNAMIC_LOSS; it++)
		{
			(pDynamicLoss++) = pow(10, atoi(argv[it+3])/20.0);	// Two input gains (args 3,4) and one headroom gain (arg 5)
			*(pDynamicLoss++) = (DSPfract)atoi(argv[it+3]);
		}*/
		/*inputLeftLoss = (argv[3]).toDouble();
		inputRightLoss = (DSPfract)atoi(argv[4]);
		inputHeadroomLoss = (DSPfract)atoi(argv[5]);*/
		inputLeftLoss = FRACT_NUM(0.5011872336272722);
		inputRightLoss = FRACT_NUM(0.5011872336272722);
		inputHeadroomLoss = FRACT_NUM(0.5011872336272722);
		pDynamicLoss = dynamic_loss;
		*(pDynamicLoss++) = inputLeftLoss;
		*(pDynamicLoss++) = inputRightLoss;
		*(pDynamicLoss) = inputHeadroomLoss;

		//outputMode = (OutputMode)atoi(argv[6]);					// Output mode (arg 6)
		outputMode = (OutputMode)argv[5];
	}

	// Initialize echo
	//-------------------------------------------------
	//multitap_echo_init();
	channel_combination_state_init();
	//-------------------------------------------------


	// Processing loop
	//-------------------------------------------------
    {
		int i;
		int j;
		int k;
		int sample;
		int bound = iNumSamples/BLOCK_SIZE;

		// exact file length should be handled correctly...
		for(i=0; i< bound; i++)
		{
			for(j=0; j<BLOCK_SIZE; j++)
			{
				for(k=0; k<nChannels; k++)
				{
					sample = cl_wavread_recvsample(wav_in);
					sampleBuffer[k][j] = rbits(sample);
				}
			}

			// Do processing...
			// Your code here...
			// Initialize echo
			//-------------------------------------------------
			//multitap_echo(sampleBuffer[0], sampleBuffer[0]);
			//channel_combination_init();

			// Call processing on first channel
			channel_combination(sampleBuffer[0], sampleBuffer[0],sampleBuffer[1], sampleBuffer[2],sampleBuffer[2], sampleBuffer[3],sampleBuffer[4], sampleBuffer[5]);
			//-------------------------------------------------

			for(j=0; j<BLOCK_SIZE; j++)
			{
				for(k=0; k<nChannels; k++)
				{
					sample = bitsr(sampleBuffer[k][j]);
					cl_wavwrite_sendsample(wav_out, sample);
				}
			}
		}
	}

	// Close files
	//-------------------------------------------------
    cl_wavread_close(wav_in);
    cl_wavwrite_close(wav_out);
	//-------------------------------------------------

    return 0;
 }
